# Sol Converter

## Context and description

On Earth, if you start a stopwatch when the Sun passes noon, and stop it when the Sun passes noon the next day, your stopwatch will show 24 hours. But if you try the same experiment on Mars, your stopwatch will show 39 seconds and 35 seconds. This is what we call a sol.

If you examine mission data from Curiosity, Perseverance or other Mars missions, all dates are expressed in sols. This makes sense in the context of a Mars mission: Because nights are so cold on Mars (down to -120°C), rovers need to wait for sunrise to start functionning, and they turn off with sunset. But as you go through the archives, you might want to know the Earth-date at which an event happened.

This program is meant to be an easy to use converter to switch between Earth-days and sols for various space missions.

## Usage

### Command line

This tool can be used from the command line to get the Earth-date associated to the sol of a given mission. Run `python main.py <mission name> <sol number>. For instance, if you want to know when Curiosity celebrated 215 sols on Mars, you can type:
```
python main.py curiosity 215
```
and this will return
```
2013-04-15 03:07:25
```

### API usage examples

To get the same result as in the previous section, use
```
from convert import sol_to_date

print(sol_to_date("curiosity", 215))
```

### Future developments

This library is still under active development. 

Currently only `"perseverance"` and `"curiosity"` are available, but more missions will soon be included.

Future functions will also include:
- A `date_to_sol` function that will function in reverse compared to `sol_to_date`
- A `now` function that will return the current sol for any Mars mission
- 

## Contributing

You can contribute by suggesting a PR at this repository. You can also create a ticket here.

## Licence

This software is distributed under the CC-BY-SA licence.
