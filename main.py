import sys
from convert import sol_to_date

if __name__ == "__main__":
    try:
        res = sol_to_date(*sys.argv[1:])
    except:
        print("I don't understand that input. Please try:")
        print("    python main.py mission_name sol_number")
    else:
        print(res)
