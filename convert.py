from datetime import datetime, timedelta

missions = {
    "perseverance": datetime(2021, 2, 18, 11, 50), #18 February 2021, 20:55 UTC
    "curiosity": datetime(2012, 9, 6, 5, 17)#6 August 2012, 05:17 UTC
}

sol_length = timedelta(hours=24, minutes=39, seconds=35) #24 hours, 39 minutes, 35 seconds

def sol_to_date(mission_name, sol_number):
    """
    Convert a number of sols in a mission into an Earth-date.

    Parameters
    ----------
    mission_name: string
        Name of the Mars mission. Exit if name is not in list of known misions.
    sol_number: int
        Number of sols that have passed since the beginning of the mission.

    Returns
    -------
    datetime
        The UTC moment when the `sol_number` sol started.
    """
    try:
        mission_start = missions[mission_name]
    except KeyError:
        print("I don't recognize that mission name. Please use one of the following:")
        for key in missions:
            print(key)
        return

    seconds_passed = int(sol_number) * sol_length.total_seconds()
    time_passed = timedelta(seconds=seconds_passed)
    earth = mission_start + time_passed

    return(earth)
